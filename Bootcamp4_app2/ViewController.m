//
//  ViewController.m
//  Bootcamp4_app2
//
//  Created by Jeremi Kaczmarczyk on 16.04.2015.
//  Copyright (c) 2015 Jeremi Kaczmarczyk. All rights reserved.
//

#import "ViewController.h"
#import "POP/POP.h"
#import "ReactiveCocoa/ReactiveCocoa.h"

@interface ViewController ()

@property(strong, nonatomic) UIView *circleThing;
@property(strong, nonatomic) NSValue *centerValue;
@property(nonatomic) BOOL offScreen;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self setUp];
    self.offScreen = YES;
    self.centerValue = [NSValue valueWithCGPoint:self.view.center];
    
    [[RACObserve(self.circleThing, center) deliverOnMainThread] subscribeNext:^(NSValue *centerValue) {
        if(!CGRectIntersectsRect(self.circleThing.frame, self.view.frame)) {
            //animate to center
            if (self.offScreen){
            self.offScreen = NO;
            NSLog(@"Dziala");
            POPBasicAnimation *move = [POPBasicAnimation animation];
            move.property = [POPAnimatableProperty propertyWithName:kPOPViewCenter];
            move.toValue = self.centerValue;
            move.duration = 0.7;
            move.completionBlock= ^void(POPAnimation *animation, BOOL completed){
                self.offScreen = YES;};
            [_circleThing pop_addAnimation:move forKey:@"move"];
            }
        }
    }];
    
    
}

- (void)setUp{
    CGFloat size = 50;
    CGRect rect = CGRectMake(self.view.center.x, self.view.center.y, size, size);
    
    self.circleThing = [[UIView alloc] initWithFrame:rect];
    [self.view addSubview:self.circleThing];
    self.circleThing.layer.cornerRadius = size/2;
    [self.circleThing setBackgroundColor:[UIColor blackColor]];
    
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc]
                                             initWithTarget:self action:@selector(handlePan:)];
    [self.circleThing addGestureRecognizer:panRecognizer];
}

- (void)handlePan:(UIPanGestureRecognizer *)panRecognizer {
    
    CGPoint touchLocation = [panRecognizer locationInView:self.view];

    
    switch (panRecognizer.state) {
        case UIGestureRecognizerStateBegan:{
            
            CGFloat size = 1.6;
            POPSpringAnimation *circleAnimation = [POPSpringAnimation animation];
            circleAnimation.property = [POPAnimatableProperty propertyWithName:kPOPViewScaleXY];
            circleAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(size, size)];
            circleAnimation.springBounciness = 10;
            circleAnimation.springSpeed = 10;
            circleAnimation.removedOnCompletion = YES;
            [_circleThing pop_addAnimation:circleAnimation forKey:@"pop"];
            //self.circleThing.layer.cornerRadius = size/2;
            
            CGFloat alpha = 0.2;
            POPSpringAnimation *alphaAnimation = [POPSpringAnimation animation];
            alphaAnimation.property = [POPAnimatableProperty propertyWithName:kPOPViewAlpha];
            alphaAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(alpha, alpha)];
            alphaAnimation.springBounciness = 10;
            alphaAnimation.springSpeed = 10;
            alphaAnimation.removedOnCompletion = YES;
            [_circleThing pop_addAnimation:alphaAnimation forKey:@"alpha"];
            
        }
            break;
        case UIGestureRecognizerStateChanged:{
            /*[UIView animateWithDuration:0.5f animations:^{
                self.circleThing.center = touchLocation;
            }];*/

            CGPoint velocity = [panRecognizer velocityInView:self.view];
            POPDecayAnimation *move = [POPDecayAnimation animation];
            move.property = [POPAnimatableProperty propertyWithName:kPOPViewCenter];
//            move.toValue = [NSValue valueWithCGPoint:touchLocation];
                move.velocity = [NSValue valueWithCGRect:CGRectMake(velocity.x, velocity.y, 50, 50)];
            move.removedOnCompletion = YES;
            [_circleThing pop_addAnimation:move forKey:@"move"];
            
            //CGPoint touchLocation = [panRecognizer locationInView:self.view];
//            POPSpringAnimation *endShake = [POPSpringAnimation animation];
//            endShake.property = [POPAnimatableProperty propertyWithName:kPOPViewCenter];
//            endShake.toValue = [NSValue valueWithCGPoint:touchLocation];
//            endShake.springBounciness = 10;
//            endShake.springSpeed = 10;
//            endShake.removedOnCompletion = YES;
//            [_circleThing pop_addAnimation:endShake forKey:@"shake"];
            }
        
            break;
        case UIGestureRecognizerStateEnded:{

            CGFloat size = 1;
            POPSpringAnimation *circleAnimation = [POPSpringAnimation animation];
            circleAnimation.property = [POPAnimatableProperty propertyWithName:kPOPViewScaleXY];
            circleAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(size, size)];
            circleAnimation.springBounciness = 10;
            circleAnimation.springSpeed = 10;
            circleAnimation.removedOnCompletion = YES;
            [_circleThing pop_addAnimation:circleAnimation forKey:@"pop"];
            //self.circleThing.layer.cornerRadius = size/2;
            
            CGFloat alpha = 1;
            POPSpringAnimation *alphaAnimation = [POPSpringAnimation animation];
            alphaAnimation.property = [POPAnimatableProperty propertyWithName:kPOPViewAlpha];
            alphaAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(alpha, alpha)];
            alphaAnimation.springBounciness = 10;
            alphaAnimation.springSpeed = 10;
            alphaAnimation.removedOnCompletion =YES;
            [_circleThing pop_addAnimation:alphaAnimation forKey:@"alpha"];
            
            //CGPoint touchLocation = [panRecognizer locationInView:self.view];
//            POPSpringAnimation *endShake = [POPSpringAnimation animation];
//            endShake.property = [POPAnimatableProperty propertyWithName:kPOPViewCenter];
//            endShake.toValue = [NSValue valueWithCGPoint:touchLocation];
//            endShake.springBounciness = 10;
//            endShake.springSpeed = 10;
//            endShake.removedOnCompletion = YES;
//            [_circleThing pop_addAnimation:endShake forKey:@"shake"];
            
    
            
            
            /*CGPoint velocity = [panRecognizer velocityInView:self.view];
            POPDecayAnimation *move = [POPDecayAnimation animation];
            move.property = [POPAnimatableProperty propertyWithName:kPOPLayerPosition];
            move.toValue = [NSValue valueWithCGPoint:velocity];
            [_circleThing pop_addAnimation:move forKey:@"move"];*/
        
        }
            break;
        case UIGestureRecognizerStateCancelled:{
          
        }
            break;
        default:
            break;
            
    }


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
